import { message } from "./index"

test("Message is 'hello world'", () =>
    expect(message).toBe("Hello, world!"));
