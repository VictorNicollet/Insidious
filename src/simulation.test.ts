import { Simulation, Entity } from './simulation'
import { Time } from './time';

function entity(step: (this: Entity, id: string, s: Simulation) => Entity): Entity {
    return { step }
}

test("Initial identifiers are ascending", () => {
    const e1 = entity(function() { return this });
    const e2 = entity(function() { return this });
    const s = new Simulation([e1, e2]);

    expect(s.entityById("0")).toBe(e1);
    expect(s.entityById("1")).toBe(e2);
    expect(() => s.entityById("2")).toThrow("No entity with id '2'")
});

test("Initial time is time(0, 0)", () =>
    expect(new Simulation([]).time.equals(new Time(0, 0))).toBe(true));

test("Time after one step is time(0, 1)", () => {
    const s = new Simulation([]);
    s.step();
    expect(s.time.equals(new Time(0, 1))).toBe(true);
});

test("Time after two steps is time(0, 2)", () => {
    const s = new Simulation([]);
    s.step();
    s.step();
    expect(s.time.equals(new Time(0, 2))).toBe(true);
});

test("During step, entities do not change", () => {

    function t(this: Entity, id: string, sim: Simulation) {
        expect(sim).toBe(s);
        expect(sim.entityById("0")).toBe(e1);
        expect(sim.entityById("1")).toBe(e2);
        return {...this, id}
    }

    const e1 = entity(t);
    const e2 = entity(t);

    const s = new Simulation([e1, e2]);
    s.step();

    expect(s.entityById("0")['id']).toBe("0");
    expect(s.entityById("1")['id']).toBe("1");
});