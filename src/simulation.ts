import { Time } from './time';

// The simulation class is a mutable model of the entire game 
// simulation. However, the entities themselves are immutable ; at the end of 
// every simulation hour, entities are replaced simultaneously with their 
// new versions.

// An entity bears an update function called on every simulation step. The 
// effects of the update are then applied at the end of the simulation step.
export type Entity = {
    readonly step: (id: string, sim: Simulation) => Entity
}

// A wrapper used for the simultaneous replacement: the next value is kept 
// around after the update step, then 
type Wrapper = {
    current: Entity
    next: Entity
}

export class Simulation {

    // The current date and time
    public time = new Time(0, 0);

    // All entities in the simulation
    private _entities : { [id: string]: Wrapper } = {}

    constructor(entities: readonly Entity[]) {

        for (let i = 0; i < entities.length; ++i) {
            const entity = entities[i];
            this._entities[i] = { current: entity, next: entity };
        }
    }

    // Compute one simulation step. 
    step() {

        // Ask all entities to step
        for (let id in this._entities) {
            const w = this._entities[id];
            w.next = w.current.step(id, this);
        }

        // Apply changes to all entities
        for (let id in this._entities) {
            const w = this._entities[id];
            w.current = w.next
        }

        // Increment the current time
        this.time = this.time.addHours(1)
    }

    // Return an entity by its identifier.
    entityById(id: string) {
        const wrapper = this._entities[id];
        if (typeof wrapper == "undefined") throw ("No entity with id '" + id + "'")
        return wrapper.current;
    }
}
